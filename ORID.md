**Objective**

- 今天是AFS培训的第一天，我们进行了破冰活动，学习了PDCA 来学会如何对一件事情进行不断迭代、持续改进、然后再次循环，也尝试创作了团队的concept map。并学习了standup meeting的技巧和ORID来进行自我反思与总结。

**Reflective**

- 今天是开心且焦虑的，因为遇到了一些未接触过的知识概念，但与此同时也为学到新知识感到开心。
- 在PDCA规划和concept map构建方面遇到了一些问题，感觉规划做得不够具体，而concept map的概念阶段收集有些困难。

**Interpretative**

- AFS培训不仅是专业知识技能的学习，同时也是是面对挑战、找出问题和解决问题的过程。
- 今天的经历使我更深入地理解了PDCA和concept map的重要性或挑战，并使我明白我需要更多的实践和思考才能更好地掌握它们。

**Decisional**

- 后续会在PDCA的规划和concept map的概念收集上多加注意，把更多的细节纳入规划中，并在概念收集阶段就尽可能完善concept map。